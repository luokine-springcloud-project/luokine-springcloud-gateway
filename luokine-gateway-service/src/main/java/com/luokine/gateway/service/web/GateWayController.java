package com.luokine.gateway.service.web;

import com.luokine.common.model.Vo.Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tianziquan
 * @create: 2019-12-24 16:35
 */
@RestController
@Slf4j
@RequestMapping("/gateway")
@Api(tags = "网关测试接口")
public class GateWayController {

    @GetMapping("/test")
    @ApiOperation("测试")
    public Resp<String> testInterface(@RequestParam Integer i){

        return Resp.ok("网关："+i);
    }

}
