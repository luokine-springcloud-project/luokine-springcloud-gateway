package com.luokine.gateway.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: tianziquan
 * @create: 2019-12-24 16:01
 */
@SpringBootApplication
//@EnableZuulProxy
@EnableDiscoveryClient
@Slf4j
public class App {
//    @Bean
//    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
//        return builder.routes()
//                .route(r -> r.path("/qq/**")
//                        .and()
//                        .uri("http://www.qq.com/"))
//                .build();
//    }
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        log.info("luokine-gateway-service----------------> is start!");
    }
}
